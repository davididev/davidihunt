﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Search for //WHEN ADD NEW PROFILE in LevelMenUI
public class TitleUI : MonoBehaviour {

    [SerializeField] GameObject mainMenu, levelMenu, creditsMenu, tipsMenu, quitButton, dailyChallengeMenu;

    private string[] creditsLines;
    private string[] tipsLines;

	// Use this for initialization
	void Start () {

        
        Time.timeScale = 1f;

        if (Application.isMobilePlatform == true)  //If and when I make an android port, allow the quit button
            quitButton.SetActive(true);
			
		if(LevelMenuUI.isDefault == true)
		{
			SetMenu(1);
		}
		ScoreData.instance().Load();
	}

    public void QuitButton()
    {
        Application.Quit();
    }

    /// <summary>
    /// Sets the menu visible
    /// </summary>
    /// <param name="id">0 = main menu, 1 = level menu, 2 = credits menu, 3 = tips menu</param>
    public void SetMenu(int id)
    {
		//Need a second for the button press sound fx to go off
        StartCoroutine(SetMenuCoroutine(id));
    }
	public void Tutorial()
    {
        LevelVars.isDailyChallenge = false;
        LoadingUI.levelToLoad = "Tutorial";
        UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
    }
	IEnumerator SetMenuCoroutine(int id)
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		mainMenu.SetActive(id == 0);
        levelMenu.SetActive(id == 1);
        creditsMenu.SetActive(id == 2);
        tipsMenu.SetActive(id == 3);
        dailyChallengeMenu.SetActive(id == 4);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
