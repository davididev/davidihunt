﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour {
    [SerializeField] protected TMPro.TMP_Text loadingText, tipText;
    [SerializeField] protected TextAsset tipTextFile;
    public static string levelToLoad = "Johnny01";
	// Use this for initialization
	void Start () {
        StartCoroutine(LolWat());
	}

    IEnumerator LolWat()
    {
        Time.timeScale = 1f;
        string[] lines = tipTextFile.text.Split('*');
        int rand = Random.Range(0, lines.Length - 1);
        tipText.text = "Tip #" + lines[rand];

        yield return new WaitForSeconds(1f);


        AsyncOperation yes = SceneManager.LoadSceneAsync(levelToLoad);
        if (yes == null)  //Level doesn't exist; load the title instead of the next level
            yes = SceneManager.LoadSceneAsync("Title");

        while(yes.isDone == false)
        {
            loadingText.text = "Loading: " + Mathf.RoundToInt(yes.progress * 100f) + "%";
            yield return new WaitForEndOfFrame();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
