﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsTextController : MonoBehaviour {
    [SerializeField] TMPro.TMP_Text creditsTxt;
    [SerializeField] TextAsset creditsTxtFile;
    // Use this for initialization
    private Coroutine yes;
    private string[] list;
    int i = 0;

	void OnEnable () {
        if (yes != null)
            StopCoroutine(yes);
        list = creditsTxtFile.text.Split('*');
        i = 0;
        UpdateText();
    }

    /// <summary>
    /// Next Button
    /// </summary>
    public void Next()
    {
        i++;
        UpdateText();
    }
    /// <summary>
    /// Previous button
    /// </summary>
    public void Previous()
    {
        i--;
        UpdateText();
    }

    /// <summary>
    /// Update the text after pressing a button or loading the panel
    /// </summary>
    void UpdateText()
    {
        i = Mathf.Clamp(i, 0, list.Length - 1);

        if (i + 1 == list.Length)
            creditsTxt.text = list[i] + "\n\nEnd of list";
        else
            creditsTxt.text = list[i] + "\n\nHit Buttons for More";
    }

	// Update is called once per frame
	void Update () {
		
	}
}
