﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Search for //WHEN ADD NEW PROFILE
//Be sure to check DailyChallengePanel.
public class LevelMenuUI : MonoBehaviour {
	public static bool isDefault = false;
	public static int lastPageNumber = 0;

    private int pageNumber = 0, profileID = 0;

    [SerializeField] private TMPro.TMP_Text[] listText;
    [SerializeField] private TMPro.TMP_Text profileText, totalScore;
    
	// Use this for initialization
	void OnEnable () {
        LevelVars.isDailyChallenge = false;
        isDefault = true;
        profileID = PlayerPrefs.GetInt("LastProfile");
        ChangeProfile();
		pageNumber = lastPageNumber;
        totalScore.text = "Total: " + ScoreData.instance().totalScore.ToString("N0");

    }
    private int totalLevels;
    /// <summary>
    /// When you change profiles, it should reset to page 0 and such.
    /// </summary>
    void ChangeProfile()
    {
		lastPageNumber = 0;
        ScoreData.instance().profileID = profileID;
        ScoreData.instance().LoadCurrentProfile();

		PlayerPrefs.SetInt("LastProfile", profileID);

        pageNumber = 0;
        //WHEN ADD NEW PROFILE
        if (profileID == ScoreData.PROFILE_JOHNNY)
        {
            totalLevels = LevelNames.JohnnyLevelNames.Length;
            profileText.text = "Johnny's Quest\n<size=20><color=#FFFF00>Easy</color></size>";
        }
        if (profileID == ScoreData.PROFILE_BECKY)
        {
            totalLevels = LevelNames.BeckyLevelNames.Length;
            profileText.text = "Becky's Quest\n<size=20><color=#FF0000>Hard</color></size>";
        }
		if (profileID == ScoreData.PROFILE_DANNY)
        {
            totalLevels = LevelNames.DannyLevelNames.Length;
            profileText.text = "Danny's Adventure\n<size=20><color=#FFFF00>Normal</color></size>";
        }
		if (profileID == ScoreData.PROFILE_SEAN)
        {
            totalLevels = LevelNames.SeanLevelNames.Length;
            profileText.text = "Sean's Journey\n<size=20><color=#FFFF68>Very Easy</color></size>";
        }
		if (profileID == ScoreData.PROFILE_DAVIDI)
        {
            totalLevels = LevelNames.DavidiLevelNames.Length;
            profileText.text = "Davidi's Journey\n<size=20><color=#FFFF68>Normal</color></size>";
        }
		
		if (profileID == ScoreData.PROFILE_NATE)
        {
            totalLevels = LevelNames.NateLevelNames.Length;
            profileText.text = "Nate's Travels\n<size=20><color=#FFFF68>Easy</color></size>";
        }
        if (profileID == ScoreData.PROFILE_TIMAEUS)
        {
            totalLevels = LevelNames.TimLevelNames.Length;
            profileText.text = "Timeaus The Wanderer\n<size=20><color=#FFFF68>Normal</color></size>";
        }

        ScoreData.IndividualScoreList si = ScoreData.instance().GetScoreList();
        int fails = 0;  //It's considered a fail if the high score is 0.  
        //Minimum of 1 points is required to go to the next level
        int i = 0;
        for (i = 0; i < (totalLevels); i++)
        {
            if (si.GetScoreOnList(i) == 0)  //First level with a score of zero should be the last level.
                fails++;
            if (fails == 2)
                break;
        }
        totalLevels = i+1;

        UpdateLevelList();
    }

    public void NextProfile()
    {
        profileID++;
        if (profileID >= ScoreData.TOTAL_PROFILES)
            profileID--;

        ChangeProfile();
    }

    public void PreviousProfile()
    {
        profileID--;
        if (profileID < 0)
            profileID++;

        ChangeProfile();
    }

    void UpdateLevelList()
    {
        int minRange = pageNumber * listText.Length;
        int maxRange = minRange + (listText.Length);

        
        //If the page number is too high
        if(minRange >= totalLevels)
        {
            pageNumber--;
            UpdateLevelList();
            return;
        }
        

        if (maxRange >= totalLevels)
            maxRange = totalLevels - 1;

        //for(int z = 0; z < listText.Length; z++)
        //{
            //listText[z].gameObject.SetActive(false);
        //}
        int x = 0;

        ScoreData.IndividualScoreList si = ScoreData.instance().GetScoreList();

        for (int i = minRange; i < maxRange; i++)
        {
            listText[x].gameObject.SetActive(true);
            string s = "";
            //WHEN ADD NEW PROFILE
            if (profileID == ScoreData.PROFILE_JOHNNY)
                s = (i+1) + ": " + LevelNames.JohnnyLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
            if (profileID == ScoreData.PROFILE_BECKY)
                s = (i + 1) + ": " + LevelNames.BeckyLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
			if (profileID == ScoreData.PROFILE_DANNY)
                s = (i + 1) + ": " + LevelNames.DannyLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
			if (profileID == ScoreData.PROFILE_SEAN)
                s = (i + 1) + ": " + LevelNames.SeanLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
			if (profileID == ScoreData.PROFILE_DAVIDI)
                s = (i + 1) + ": " + LevelNames.DavidiLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
			if (profileID == ScoreData.PROFILE_NATE)
                s = (i + 1) + ": " + LevelNames.NateLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3") 
					+ "(" +  ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
            if (profileID == ScoreData.PROFILE_TIMAEUS)
                s = (i + 1) + ": " + LevelNames.TimLevelNames[i] + "\n<size=18>High Score: " + si.GetScoreOnList(i).ToString("D3")
                    + "(" + ScoreData.GetGrade(si.GetScoreOnList(i)) + ")</size>";
            listText[x].text = s;
            x++;
        }
		for(int i = x; i < listText.Length; i++)
		{
			listText[i].gameObject.SetActive(false);
		}
    }

    /// <summary>
    /// Press the down page button
    /// </summary>
    public void NextPage()
    {
        pageNumber++;
        UpdateLevelList();
		lastPageNumber = pageNumber;
    }

    /// <summary>
    /// Press the up page button
    /// </summary>
    public void PrevPage()
    {
        pageNumber--;
        if (pageNumber < 0)
            pageNumber = 0;
        UpdateLevelList();
		lastPageNumber = pageNumber;

    }

    /// <summary>
    /// Press the individual level button
    /// </summary>
    /// <param name="buttonID">Button ID</param>
    public void PressLevelButton(int buttonID)
    {
        int x = pageNumber * listText.Length;
        x += buttonID;
        x += 1;

        string levelToLoad = "";
        //WHEN ADD NEW PROFILE
        if (ScoreData.instance().profileID == ScoreData.PROFILE_JOHNNY)
            levelToLoad = "Johnny" + x.ToString("D2");
        if (ScoreData.instance().profileID == ScoreData.PROFILE_BECKY)
            levelToLoad = "Becky" + x.ToString("D2");
		if (ScoreData.instance().profileID == ScoreData.PROFILE_DANNY)
            levelToLoad = "Danny" + x.ToString("D2");
		if (ScoreData.instance().profileID == ScoreData.PROFILE_SEAN)
            levelToLoad = "Sean" + x.ToString("D2");
		if (ScoreData.instance().profileID == ScoreData.PROFILE_DAVIDI)
            levelToLoad = "Davidi" + x.ToString("D2");
		if (ScoreData.instance().profileID == ScoreData.PROFILE_NATE)
            levelToLoad = "Nate" + x.ToString("D2");
        if (ScoreData.instance().profileID == ScoreData.PROFILE_TIMAEUS)
            levelToLoad = "Tim" + x.ToString("D2");
        LoadingUI.levelToLoad = levelToLoad;
        SceneManager.LoadScene("Loading");
    }

    // Update is called once per frame
    void Update () {

        //if (Input.GetKeyDown(KeyCode.Delete))
            //PlayerPrefs.DeleteAll();
	}
}
