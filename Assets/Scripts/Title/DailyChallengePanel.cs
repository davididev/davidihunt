﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Search for //WHEN ADD NEW PROFILE
public class DailyChallengePanel : MonoBehaviour {

    private string characterID, sceneName, levelName;
    private int levelID, highScore, seed;

    public TMPro.TextMeshProUGUI textInfo;

	// Use this for initialization
	void OnEnable () {
        seed = System.DateTime.Now.DayOfYear;
        Random.InitState(seed);
        
        int dailyChallenge = Random.Range(3, SceneManager.sceneCountInBuildSettings - 1);
        string s = SceneUtility.GetScenePathByBuildIndex(dailyChallenge);


        sceneName = s.Substring(s.LastIndexOf('/') + 1);
        sceneName = sceneName.Substring(0, sceneName.Length - 6);  //Remove the .unity
        textInfo.text = sceneName;
        characterID = sceneName.Substring(0, length: sceneName.Length - 2);
        levelID = int.Parse(sceneName.Substring(sceneName.Length - 2));


        ExtractCharacter();
        GetHighScore();
        textInfo.text = "---Today's challenge---\nCharacter: <color=yellow>" + characterID + 
            "</color>\nLevel: <color=yellow>" + levelName 
            + "</color>\nCurrent score: <color=yellow>" + highScore + "</color>";

    }

    void GetHighScore()
    {
        highScore = 0;
        if(PlayerPrefs.GetInt("DC_date") == seed)
        {
            //Playing for a second time today
            highScore = PlayerPrefs.GetInt("DC_score");
        }
        else
        {
            //Playing for the first time today.
            PlayerPrefs.SetInt("DC_score", 0);
            PlayerPrefs.SetInt("DC_date", seed);
        }

    }

    public void PlayChallenge()
    {
        LevelVars.isDailyChallenge = true;
        LoadingUI.levelToLoad = sceneName;
        SceneManager.LoadScene("Loading");
    }

    //WHEN ADD NEW PROFILE
    void ExtractCharacter()
    {
        int i = levelID - 1;  //levelID prints to the Scene#, which starts at 1 instead of 0.
        if (characterID == "Becky")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_BECKY;
            levelName = LevelNames.BeckyLevelNames[i];
        }
        if (characterID == "Danny")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_DANNY;
            levelName = LevelNames.DannyLevelNames[i];
        }
        if (characterID == "Davidi")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_DAVIDI;
            levelName = LevelNames.DavidiLevelNames[i];
        }
        if (characterID == "Johnny")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_JOHNNY;
            levelName = LevelNames.JohnnyLevelNames[i];
        }
        if (characterID == "Nate")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_NATE;
            levelName = LevelNames.NateLevelNames[i];
        }
        if (characterID == "Sean")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_SEAN;
            levelName = LevelNames.SeanLevelNames[i];
        }
        if (characterID == "Tim")
        {
            ScoreData.instance().profileID = ScoreData.PROFILE_TIMAEUS;
            levelName = LevelNames.TimLevelNames[i];
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
