﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelNames  {

    public static string[] JohnnyLevelNames =
    {
        "Green Slimes Galore",
        "Mine Of the Spikers",
        "Black Fog Forest",
		"Snowy City",
		"Slimy Rainforest",
		"Foggy Castle",
		"Zero-G Temple"
    };

    public static string[] BeckyLevelNames =
    {
        "The Meeting Area",
        "Thirsty Desert",
        "Electrical Maze",
		"Blue Forest",
		"Cave of Darkness",
		"Temple Of Chaos",
		"Green and Blue Desert",
		"Turtle Castle",
		"Not so wet ruins",
		"Ice Palace",
		
		
    };
	
	public static string[] DannyLevelNames =
	{
		"Deck 18",
		"Tri Gateway",
		"Shangra Lee",
        "Tree Forest",
		"Deimo Moon",
		"Hyper Burst",
		"Death Trap"
	};
	
	
	public static string[] SeanLevelNames =
	{
		"Enter The Sewer",
		"Forest Town",
		"Baron Castle",
		"Dark Town"
	};
	
	public static string[] DavidiLevelNames =
	{
		"Ghost Cemetery",
		"Electrical Temple",
		"Snowy Volcano"
		
	};
	
	public static string[] NateLevelNames =
	{
		"Snowy Temple",
		"Rain Forest",
		"Red Temple",
        "Explosion Sewer"
	};

    public static string[] TimLevelNames =
    {
        "Da Mooooon",
        "SPACESHIP!!!",
        "Saturn",
		"The Magma Sewer",
		"Forest of Explosions",
		"Sparkle Dungeon"
    };
}
