﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreData {

    public int profileID = PROFILE_TIMAEUS;

    public const int TOTAL_PROFILES = 7;
    public const int PROFILE_JOHNNY = 0;
    public const int PROFILE_BECKY = 1;
	public const int PROFILE_DANNY = 2;
	public const int PROFILE_SEAN = 3;
	public const int PROFILE_DAVIDI = 4;
	public const int PROFILE_NATE = 5;
    public const int PROFILE_TIMAEUS = 6;

    public class IndividualScoreList
    {
        protected int[] list;
        public IndividualScoreList()
        {
            list = new int[99];
            for(int i = 0; i < list.Length; i++)
            {
                list[i] = 1;
            }
        }
		
		public int[] GetList()
		{
			return list;
		}

        /// <summary>
        /// Get individual score
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetScoreOnList(int id)
        {
            return list[id];
        }

        /// <summary>
        /// Return all the scores for this profile
        /// </summary>
        /// <returns></returns>
        public int GetTotalScore()
        {
            int t = 0;
            for(int i = 0; i < list.Length; i++)
            {
                t += list[i];
            }
            return t;
        }


        public int GetIndivScore(int levelID)
		{
			return list[levelID];
		}
        /// <summary>
        /// Set the score of an indivudal level.  Only sets if it's a new high score.
        /// </summary>
        /// <param name="levelID">Which level are we setting the score to?</param>
        /// <param name="score">What is the score of the level?</param>
        public void SetIndivScore(int levelID, int score)
        {
            if(list[levelID] < score)
                list[levelID] = score;
        }

        /// <summary>
        /// Load a list based on a string (PlayerPrefs)
        /// </summary>
        /// <param name="s">string to laod the list</param>
        public void LoadList(string s)
        {
            string[] nums = s.Split(',');
            for(int i = 0; i < list.Length; i++)
            {
                if (i < nums.Length)
                {
                    int lol = 0;
                    //Debug.Log("parsing " + nums[i]);
                    if (int.TryParse(nums[i], out lol) == true)  //Parse succeeded.
                        list[i] = lol;
                    else //Failed to parse; set it to default
                        list[i] = 1;
                    
                }
                else  //No data written; set it to 1.
                    list[i] = 1;
            }
        }

        /// <summary>
        /// Converts the score list to a string.
        /// </summary>
        /// <returns></returns>
        public string SaveList()
        {
            string s = "";
            for(int i = 0; i < list.Length; i++)
            {
                s = s + list[i] + ",";
            }

            return s;
        }
    }

    public int totalScore { private set; get; }
    private static ScoreData _instance;
    /// <summary>
    /// Get the instance.
    /// </summary>
    /// <returns></returns>
    public static ScoreData instance()
    {
        if (_instance == null)
            _instance = new ScoreData();

        return _instance;
    }


    private IndividualScoreList[] scores;

    /// <summary>
    /// Get a string letter grade of the score
    /// </summary>
    /// <param name="total"></param>
    /// <returns></returns>
    public static string GetGrade(int total)
	{
		string grade = "<color=#FF0000>F</color>";
		if(total >= 50)
			grade = "<color=#FF6868>D</color>";
		if(total >= 150)
			grade = "<color=#0048FF>C</color>";
		if(total >= 300)
			grade = "<color=#00CC00>B</color>";
		if(total >= 350)
			grade = "<color=#00FF00>A</color>";
		if(total > 400)
			grade = "<color=#00FF00>A+</color>";
		
		return grade;
	}
	

	public ScoreData()
    {
		
        scores = new IndividualScoreList[TOTAL_PROFILES];
        for(int i = 0; i < scores.Length; i++)
        {
            scores[i] = new IndividualScoreList();
        }
		
		
    }
	
	public void Load()
	{
		string[] lines = CSVReader.GetFile(Application.persistentDataPath + "/data.txt");
		if(lines != null)
		{
			for(int i = 0; i < lines.Length; i++)
			{
				object[] args = CSVReader.GetArgumentsInLine(lines[i]);
				string s = (string) args[0];
				if(s.Length > 0 && s[0] == 'p')
				{
					int id = int.Parse(s.Substring(1));
					int[] list = CSVUtil.StringToIntList((string) args[1], 99);
					for(int x = 0; x < list.Length; x++)
					{
						scores[id].SetIndivScore(x, list[x]);
					}
				}	
				
			}
			GetTotalScore();
		}
		
		Application.ExternalCall("SyncFiles"); 
	}

    public IndividualScoreList GetScoreList()
    {
        return scores[profileID];
    }
	
	public int GetCurrentHighScore()
	{
		int x = UIController.ExtractLevelID() - 1;
		return scores[profileID].GetIndivScore(x);
	}

    /// <summary>
    /// Save the profile; should be called at the end of a scene.
    /// </summary>
    public void SaveCurrentProfile()
    {
        int x = UIController.ExtractLevelID() - 1;
        scores[profileID].SetIndivScore(x, LevelScore.getInstance().TotalScore);

        GetTotalScore();

        //PlayerPrefs.SetString("Pro" + profileID, scores[profileID].SaveList());
        Debug.Log("Saving Pro" + profileID + ": " + scores[profileID].SaveList());
        //PlayerPrefs.Save();
		Save();
    }
	
	public void Save()
	{
		CSVWriter.Reset();
		for(int i = 0; i < TOTAL_PROFILES; i++)
		{
			CSVWriter.WriteLine(new object[] { ("p" + i), CSVUtil.IntListToString(scores[i].GetList(), 99) } );
		}
		GetTotalScore();
		CSVWriter.WriteLine(new object[] { "tot", totalScore} );
		
		CSVWriter.SaveFile(Application.persistentDataPath + "/data.txt");
		Application.ExternalCall("SyncFiles"); 
	}

    /// <summary>
    /// Update the total score.
    /// </summary>
    public void GetTotalScore()
    {
        int total = 0;
        for(int i = 0; i < scores.Length; i++)
        {
            total += scores[i].GetTotalScore();
        }

        totalScore = total;
    }

    public void LoadCurrentProfile()
    {
        //Debug.Log("Loading Pro" + profileID + ": " + PlayerPrefs.GetString("Pro" + profileID));
        //if()
        //{
            //New profile
            //scores[profileID] = new IndividualScoreList();
        //}
       // else
        //{
            //Used profile
            //Debug.Log("You're a farty pants.");
            //scores[profileID].LoadList(PlayerPrefs.GetString("Pro" + profileID));
        //}

       // GetTotalScore();


    }

    
}
