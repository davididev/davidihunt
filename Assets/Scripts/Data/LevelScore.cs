﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScore {

    public int _zaps = 0;  //How many times you pressed the fire button
    public int _successZaps = 0;  //How many times you successfully zapped an enemy
    
    public int _health = 100;

    public int _maxTimer = 200;
    public int _currentTimer = 200;

    public int _enemiesSlain = 0;
    public int _totalEnemiesSlain = 0;

    public int AccuracyScore
    {
        get
        {
            if (_zaps == 0)
                return 0;
            return _successZaps * 100 / _zaps;
        }
    }

    public int HealthScore
    {
        get { return _health; }
    }

    public int TimerScore
    {
        get {
            if (_maxTimer == 0)
                return 100;
            //Return 200% of timer, but don't go over 100
            return Mathf.Clamp(_currentTimer * 100 * 2 / _maxTimer, 0, 100);
        }
    }

    public int EnemyScore
    {
        get {
            if (_totalEnemiesSlain == 0)
                return 0;
            return _enemiesSlain * 100 / _totalEnemiesSlain; }
    }
    public int TotalScore
    {
        get
        {
            return EnemyScore + TimerScore + HealthScore + AccuracyScore;
        }
    }

    /// <summary>
    /// Start a new level with certain time (called by LevelVars)
    /// </summary>
    /// <param name="timer"></param>
    public void ResetVars(int timer)
    {
        _maxTimer = timer;
        _currentTimer = _maxTimer;
        _health = 100;
        _enemiesSlain = 0;
        _totalEnemiesSlain = 0;
        _zaps = 0;
        _successZaps = 0;
    }

    private static LevelScore instance;

    public static LevelScore getInstance()
    {
        if (instance == null)
            instance = new LevelScore();

        return instance;
    }
}
