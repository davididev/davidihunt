﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRun : MonoBehaviour {

    [SerializeField] private ParticleSystem[] systems;
    private float totalTime = 0f;
	// Use this for initialization
	void OnEnable () {
        foreach (ParticleSystem p in systems)
        {
            p.Play();
        }

    }


    // Update is called once per frame
    void Update () {
        bool isPlaying = false;
	    foreach(ParticleSystem p in systems)
        {
            if (p.isPlaying == true)
                isPlaying = true;
        }

        if(isPlaying == false)
        {
            gameObject.SetActive(false);
        }
	}
}
