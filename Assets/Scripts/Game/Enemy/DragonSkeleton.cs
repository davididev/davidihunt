﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonSkeleton : Enemy {

    public Vector2 startFacing;
    BulletManager bullets;
    private const float FIRE_DELAY = 0.25f;

    protected override IEnumerator MainFunction()
    {
        bullets = GetComponent<BulletManager>();

        control.SetDirection(startFacing);
        control.lockFacing = true;
        Transform player = GameObject.FindWithTag("Player").transform;
        float fireTimer = 0f;
        while(gameObject.activeInHierarchy)
        {
            if(startFacing.x == 0f)
            {
                if (transform.position.x < player.position.x)
                    control.SetDirection(new Vector2(1, 0f));
                if (transform.position.x > player.position.x)
                    control.SetDirection(new Vector2(-1f, 0f));
            }
            if (startFacing.y == 0f)
            {
                if (transform.position.z < player.position.z)
                    control.SetDirection(new Vector2(0f, 1f));
                if (transform.position.z > player.position.z)
                    control.SetDirection(new Vector2(0f, -1f));
            }
            fireTimer -= Time.deltaTime;
            if(ScanForPlayer() == true)
            {
                if (fireTimer <= 0)
                {
                    bullets.SpawnABullet(0, transform.position + cc.center, control.angle);
                    fireTimer = FIRE_DELAY;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
