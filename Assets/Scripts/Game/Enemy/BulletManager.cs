﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour {

    [System.Serializable]
    public struct BulletEntry
    {
        public GameObject prefab;
        [HideInInspector] public GameObject[] list;
        public int poolSize;
    }

    public BulletEntry[] listOfBullets;

	// Use this for initialization
	void Start () {
		for(int i = 0; i < listOfBullets.Length; i++)
        {
            listOfBullets[i].list = new GameObject[listOfBullets[i].poolSize];
            for(int x = 0; x < listOfBullets[i].list.Length; x++)
            {
                listOfBullets[i].list[x] = Instantiate(listOfBullets[i].prefab, transform.position, Quaternion.identity) as GameObject;
                listOfBullets[i].list[x].SetActive(false);
            }
        }
	}

    public void SpawnABullet(int id, Vector3 spawnPos, float angle)
    {
        for(int x = 0; x < listOfBullets[id].list.Length; x++)
        {
            if(listOfBullets[id].list[x].activeInHierarchy == false)
            {
                GameObject g = listOfBullets[id].list[x];
                g.transform.position = spawnPos;
                Vector3 ang = g.transform.eulerAngles;
                ang.y = angle;
                g.transform.eulerAngles = ang;
                g.SetActive(true);
                break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
