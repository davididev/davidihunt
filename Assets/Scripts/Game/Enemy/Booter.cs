﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booter : Enemy {

    private BulletManager bm;

    public override void OnStart()
    {
        base.OnStart();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    /// <summary>
    /// Shoot up, down, left right
    /// </summary>
    void ShootStraight()
    {
        Vector3 center = transform.position + cc.center;
        bm.SpawnABullet(0, center, 0f);
        bm.SpawnABullet(0, center, 90f);
        bm.SpawnABullet(0, center, 180f);
        bm.SpawnABullet(0, center, 270f);
    }

    void ShootDiagnol()
    {
        Vector3 center = transform.position + cc.center;
        bm.SpawnABullet(0, center, 45f);
        bm.SpawnABullet(0, center, 135f);
        bm.SpawnABullet(0, center, 225f);
        bm.SpawnABullet(0, center, 315f);
    }

    protected override IEnumerator MainFunction()
    {
        bm = GetComponent<BulletManager>();
        while (gameObject.activeInHierarchy)
        {
            SetBrain(BRAIN.STAND_STILL);

            float delay = 1.5f;

            control.SetDirection(Vector2.up);
            ShootStraight();
            yield return new WaitForSeconds(delay);
            control.SetDirection(Vector2.right);
            ShootDiagnol();
            yield return new WaitForSeconds(delay);
            control.SetDirection(Vector2.down);
            ShootStraight();
            yield return new WaitForSeconds(delay);
            control.SetDirection(Vector2.left);
            ShootDiagnol();
            yield return new WaitForSeconds(delay);

            SetBrain(BRAIN.FOLLOW_PLAYER);
            yield return new WaitForSeconds(delay * 1.25f);
        }
    }
}
