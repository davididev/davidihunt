﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    protected Health h;
    protected SpriteController control;
    protected CharacterController cc;
    public float scanDist = 4f;
    public int addedWeight = 1;
    private Coroutine brain;
    protected float startingSpeed;
    public enum BRAIN { STAND_STILL, PACE_RANDOM, FOLLOW_PLAYER, FLEE_PLAYER, CUSTOM}
    Transform deathPrefab;
    public int touchDamage = 5;
    [SerializeField] protected AudioClip dieSoundFX;

	[SerializeField] GameObject blobShadowPrefab;
	protected GameObject blobShadowInstance;
	
    // Use this for initialization
    void Start () {
        h = GetComponent<Health>();
        control = GetComponent<SpriteController>();
        cc = GetComponent<CharacterController>();
        startingSpeed = control.speed;

        StartCoroutine(Counter());

        deathPrefab = transform.GetChild(0);
        transform.DetachChildren();

        OnStart();
        StartCoroutine(MainFunction());
		
		if(blobShadowPrefab != null)
			blobShadowInstance = GameObject.Instantiate(blobShadowPrefab, transform.position, blobShadowPrefab.transform.rotation) as GameObject;
	}

    IEnumerator Counter()
    {
        yield return new WaitForEndOfFrame();
        LevelScore.getInstance()._totalEnemiesSlain += addedWeight;
    }

    protected bool SphereScan(float scanRadius)
    {
        bool foundPlayer = false;
        
            Collider[] cols = Physics.OverlapSphere(transform.position, scanRadius, LayerMask.GetMask("Player"));
            if (cols.Length > 0)
            {
                foundPlayer = true;
            }

        return foundPlayer;
    }

    public void OnDeath()
    {
        LevelScore.getInstance()._enemiesSlain += addedWeight;
        if(dieSoundFX != null)
            AudioSource.PlayClipAtPoint(dieSoundFX, Camera.main.transform.position);
        deathPrefab.gameObject.SetActive(true);
        deathPrefab.position = transform.position;
		
		if(blobShadowInstance != null)
			blobShadowInstance.SetActive(false);
    }

	
	// Update is called once per frame
	void Update () {
        if(h == null)
            h = GetComponent<Health>();
        if(control == null)
            control = GetComponent<SpriteController>();
        if(cc == null)
            cc = GetComponent<CharacterController>();
        
		if(blobShadowInstance != null)
		{
			blobShadowInstance.transform.position = transform.position + new Vector3(0f, 5f, 0f);
			blobShadowInstance.transform.eulerAngles = new Vector3(90f, 0f, 0f);
		}
		
		OnUpdate();
	}

    public virtual void OnStart()
    {

    }

    public virtual void OnUpdate()
    {

    }

    protected void SetBrain(BRAIN b)
    {
        Debug.Log("Cookie");
        if (brain != null)
            StopCoroutine(brain);

        if (b == BRAIN.STAND_STILL)
            control.speed = 0;

        if (b == BRAIN.PACE_RANDOM)
            brain = StartCoroutine(BrainRandomPacing());
        if (b == BRAIN.FOLLOW_PLAYER)
            brain = StartCoroutine(BrainFollowPlayer());

    }

    private IEnumerator BrainFollowPlayer()
    {
        Debug.Log(gameObject.name + " is now following player.");
        Transform playerTrans = GameObject.FindWithTag("Player").transform;
        while(gameObject)
        {
            control.speed = startingSpeed;
            Vector2 v = new Vector2(0f, 0f);
            Vector3 rel = playerTrans.position - transform.position;
            if(Mathf.Abs(rel.x) > Mathf.Abs(rel.z))  //Go left/right
            {
                if (rel.x > 0f)
                    v.x = 1f;
                else
                    v.x = -1f;
            }
            else  //Go up/down
            {
                if (rel.z > 0f)
                    v.y = 1f;
                else
                    v.y = -1f;
            }


            control.SetDirection(v);
            yield return new WaitForSeconds(0.25f);
        }
    }

    private IEnumerator BrainFleePlayer()
    {
        Debug.Log(gameObject.name + " is now fleeing player.");
        Transform playerTrans = GameObject.FindWithTag("Player").transform;
        while (gameObject)
        {
            control.speed = startingSpeed;
            Vector2 v = new Vector2(0f, 0f);
            Vector3 rel = transform.position - playerTrans.position;
            if (Mathf.Abs(rel.x) > Mathf.Abs(rel.z))  //Go left/right
            {
                if (rel.x > 0f)
                    v.x = 1f;
                else
                    v.x = -1f;
            }
            else  //Go up/down
            {
                if (rel.z > 0f)
                    v.y = 1f;
                else
                    v.y = -1f;
            }


            control.SetDirection(v);
            yield return new WaitForSeconds(0.25f);
        }
    }

    /// <summary>
    /// Look for player based on current facing.  Returns true if player is found; returns false otherwise
    /// </summary>
    protected bool ScanForPlayer(float dist)
    {
        Vector3 origin = transform.position + cc.center;
        LayerMask lm = LayerMask.GetMask("Default", "Player", "Lava", "Water");
        Vector3 dir = control.GetForwardRay();

        RaycastHit hitInfo;
        if(Physics.Raycast(origin, dir, out hitInfo, dist, lm))
        {
            if(hitInfo.transform.tag == "Player")
            {
                return true;
            }
        }

        return false;
    }

    protected bool ScanForPlayer()
    {
        return ScanForPlayer(scanDist);
    }

    /// <summary>
    /// Brain- moves in a random direction and then returns to center
    /// </summary>
    /// <returns></returns>
    private IEnumerator BrainRandomPacing()
    {
        Debug.Log(gameObject.name + " is now pacing.");
        Vector3 startingPos = transform.position;


        while (gameObject)
        {
            Vector2 v = new Vector2(0f, 0f);
            int random = Random.Range(1, 4);
            if (random == 1)
                v.Set(0f, 1f);
            if (random == 2)
                v.Set(1f, 0f);
            if (random == 3)
                v.Set(0f, -1f);
            if (random == 4)
                v.Set(-1f, 0f);
            control.SetDirection(v);
            control.speed = 0;
            yield return new WaitForSeconds(1f);
            control.speed = startingSpeed;
            yield return new WaitForSeconds(0.5f);


            //Reverse direction so it goes back to starting pos.
            v = v * -1f;
            control.SetDirection(v);
            control.speed = 0;
            yield return new WaitForSeconds(1f);
            control.speed = startingSpeed;
            yield return new WaitForSeconds(0.5f);
        }
    }

    /// <summary>
    /// The main coroutine that controls the enemy.  Should be set in superclass
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator MainFunction()
    {
        yield return null;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.transform.tag == "Player")
        {
            hit.transform.GetComponent<Health>().Damage(touchDamage);
        }
    }

    void OnHit(ControllerCollisionMessage hit)
    {
        if(hit.caster.tag == "Player")  
        {
            hit.caster.GetComponent<Health>().Damage(touchDamage);
        }
    }
}
