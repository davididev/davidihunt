﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Enemy {

    protected override IEnumerator MainFunction()
    {
        SetBrain(BRAIN.PACE_RANDOM);
        bool foundPlayer = false;
        while(foundPlayer == false)
        {
            foundPlayer = ScanForPlayer();
            if (h.healthPerc < 1f)  //If it gets hurt before it stumbles naturally.
                foundPlayer = true;
            yield return new WaitForEndOfFrame();
        }

            SetBrain(BRAIN.FOLLOW_PLAYER);

        while (h.healthPerc > 0.25f)
        {
            yield return new WaitForEndOfFrame();
        }

        SetBrain(BRAIN.FLEE_PLAYER);
        while(gameObject.activeInHierarchy)
        {
            yield return new WaitForEndOfFrame();
        }
    }
}
