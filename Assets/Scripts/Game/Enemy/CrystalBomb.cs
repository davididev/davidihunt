﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBomb : Enemy {


    public float scanRadius = 10f;
    protected const float NEAR_RADIUS = 2.75f;
	public AudioClip explosionSound;

    public GameObject explosionPrefab;

    public override void OnStart()
    {
        base.OnStart();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    protected override IEnumerator MainFunction()
    {
        yield return base.MainFunction();
        this.SetBrain(BRAIN.PACE_RANDOM);
        bool found = false;
        while(found == false)
        {
            found = SphereScan(scanRadius);
            yield return new WaitForEndOfFrame();
        }

		this.SetBrain(BRAIN.FOLLOW_PLAYER);
		
        found = false;
        while (found == false)
        {
            found = SphereScan(NEAR_RADIUS);
            yield return new WaitForEndOfFrame();
        }

		AudioSource.PlayClipAtPoint(explosionSound, transform.position);
        GameObject explosion = GameObject.Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
        GameObject.FindWithTag("Player").GetComponent<Health>().Damage(20);

		blobShadowInstance.SetActive(false);
        gameObject.SetActive(false);
    }
}
