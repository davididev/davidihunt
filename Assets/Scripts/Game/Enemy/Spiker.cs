﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spiker : Enemy {

    public int currentFacing = 1;

    public override void OnStart()
    {
        currentFacing--;  //It's about to be incremented
        ChangeCurrentFacing();
        base.OnStart();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    protected override IEnumerator MainFunction()
    {
        yield return base.MainFunction();
        SetBrain(BRAIN.CUSTOM);
        //This is a dumb enemy, but it needs a main function sadly :(
        while(gameObject.activeInHierarchy)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    void ChangeCurrentFacing()
    {
        currentFacing++;
        if (currentFacing > 4)
            currentFacing = 1;

        if(currentFacing == 1)
            control.SetDirection(Vector2.up);
        if(currentFacing == 2)
            control.SetDirection(Vector2.right);
        if (currentFacing == 3)
            control.SetDirection(Vector2.down);
        if (currentFacing == 4)
            control.SetDirection(Vector2.left);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        
        if(hit.normal.y < 1f)  //It's a wall or an enemy!
        {
            Debug.Log("Normal of " + gameObject.name + ": " + hit.normal);
            //if(hit.gameObject.layer == LayerMask.NameToLayer("Default"))  //It IS a wall!
            //{
            ChangeCurrentFacing();
            //}
        }
    }
}
