﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy {

    public float speedIdle = 1f;
    public float speedDash = 4f;


    private Vector2 targetPos;

	
	
	
    protected override IEnumerator MainFunction()
    {
        yield return base.MainFunction();
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        while (gameObject.activeSelf)
        { 
            bool targetFound = false;
            control.speed = speedIdle;
            SetBrain(BRAIN.FOLLOW_PLAYER);
            while(targetFound == false)
            {
				float upOrDown = 0f;
				if(player.transform.position.y < transform.position.y)
					upOrDown = -0.75f;
				if(player.transform.position.y > transform.position.y)
					upOrDown = 0.75f;
				cc.Move(Vector3.up * upOrDown * Time.fixedDeltaTime);
				
                targetFound = this.ScanForPlayer();
                if (targetFound == true)
                {
                    Vector3 rel = transform.position - player.transform.position;
                    if(Mathf.Abs(rel.x) > Mathf.Abs(rel.z))
                    {
                        //Target to the left/right
                        targetPos = new Vector2(player.transform.position.x, transform.position.z);
                    }
                    else
                    {
                        //Target to up/down
                        targetPos = new Vector2(transform.position.x, player.transform.position.z);
                    }
                    targetPos = new Vector2(player.transform.position.x, player.transform.position.z);
                    

                }
                yield return new WaitForEndOfFrame();
            }

            SetBrain(BRAIN.CUSTOM);

            control.speed = speedDash;
            float dist = 100f;
            while (dist > 0.1f)
            {
                dist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), targetPos);
                if (dist < 0.75f)  //Slow down when you get close.
                    control.speed = speedIdle;
                Vector2 v = new Vector2(targetPos.x - transform.position.x, targetPos.y - transform.position.z);
                control.SetDirection(v);
                yield return new WaitForEndOfFrame();
            }

        }
    }


}
