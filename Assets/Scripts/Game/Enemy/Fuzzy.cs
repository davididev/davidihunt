﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuzzy : Enemy {

    public float scanRadius = 5f, jumpHeight = 3f;

    public override void OnStart()
    {
        base.OnStart();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    private IEnumerator Hop()
    {
        while(gameObject)
        {
            if (this.control.isGrounded == true)
            {
                Debug.Log("Grounded");
                this.control.Jump(jumpHeight);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    protected override IEnumerator MainFunction()
    {
        yield return base.MainFunction();
        StartCoroutine(Hop());
        SetBrain(BRAIN.STAND_STILL);

        bool found = false;
        while (found == false)
        {
            found = SphereScan(scanRadius);
            yield return new WaitForEndOfFrame();
        }

        SetBrain(BRAIN.FOLLOW_PLAYER);
        while(gameObject)
        {
            yield return new WaitForEndOfFrame();
        }
    }
}
