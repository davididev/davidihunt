﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    [SerializeField] private Image healthBarOverlay;
    [SerializeField] private RectTransform endingScore;
    [SerializeField] private TMPro.TMP_Text endingScoreText, timerText, powerupText, speedText;
    [SerializeField] private AudioClip scoreFX;
    [SerializeField] private GameObject nextLevelButton, replayLevelButton, returnToTitleButton, pauseMenu;
	[SerializeField] private GameObject[] powerupsCover;
	[SerializeField] private Image[] powerupsBar;
    private Coroutine timerCoroutine, endingScoreCoroutine;

    // Use this for initialization
    void Start () {
        timerCoroutine = StartCoroutine(Timer());
	}
	
	public void ShowPowerupText(int id)
	{
		string s = Powerup.GetPowerupString(id);
		powerupText.text = s;
		powerupText.GetComponent<Animator>().SetTrigger("PowerupAlive");
	}
	
	public void EndPowerupText(int id)
	{
		string s = Powerup.GetPowerupString(id) + " gone";
		powerupText.text = s;
		powerupText.GetComponent<Animator>().SetTrigger("PowerupDead");
	}
	
	
    public void TogglePauseMenu()
    {
        if (endingScoreCoroutine != null) //Level is complete- no pause
            return;

        bool isPaused = pauseMenu.activeInHierarchy;
        if(isPaused == true)  //Unpause
        {
            Time.timeScale = 1f;
            pauseMenu.SetActive(false);
        }
        else  //Pause
        {
            Time.timeScale = 0f;
            pauseMenu.SetActive(true);
        }
    }

    public void RestartLevel()
    {

        LoadingUI.levelToLoad = SceneManager.GetActiveScene().name;
        PlayerPrefs.Save();
		SceneManager.LoadScene("Loading");
		
    }

    public void QuitGame()
    {
        LoadingUI.levelToLoad = "Title";
        SceneManager.LoadScene("Loading");
    }

	// Update is called once per frame
	void Update () {
        healthBarOverlay.fillAmount = (float)LevelScore.getInstance()._health / 100f;
        Color c = Color.green;
        if (LevelScore.getInstance()._health <= 25)
            c = Color.red;

        healthBarOverlay.color = c;
		
		
		for(int i = 0;  i < powerupsCover.Length; i++)
		{
			float myPerc = PlayerController.Powerup_Perc[i];
			powerupsCover[i].SetActive(myPerc > 0f);
			powerupsBar[i].fillAmount = myPerc;
			
			Color c2 = Color.green;
			if(myPerc < 0.25f)
				c2 = Color.red;
			
			powerupsBar[i].color = c2;
			
			
		}
		
		if(Time.timeScale > 1f)
			speedText.text = "Double Speed On";
		else
			speedText.text = "<color=#cccccc>Double Speed Off</color>";

        /*
        if (Input.GetKeyDown(KeyCode.S))
        {
            StartCoroutine(EndingScoreOverlay());
        }
        */

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    public static int ExtractLevelID()
    {
        string levelName = SceneManager.GetActiveScene().name;
        int x;
        if (int.TryParse(levelName.Substring(levelName.Length - 2, 2), out x))
        {
            return x;
        }
        else
            return -1;
    }

    public static string ExtractCharacterName()
    {
        string levelName = SceneManager.GetActiveScene().name;
        return levelName.Substring(0, levelName.Length - 2);
    }

    public void FinishedDailyChallenge()
    {
        
        //Save score?
    }

    public void GoToNextLevel()
    {
        int levelID = ExtractLevelID();
        string characterName = ExtractCharacterName();
        Debug.Log("Current level: " + characterName + "()" + levelID);


        if(levelID != -1)
            ScoreData.instance().SaveCurrentProfile();

        levelID++;
        string newLevel = characterName + levelID.ToString("D2");
        Debug.Log("New Level: " + newLevel);

        LoadingUI.levelToLoad = newLevel;
        SceneManager.LoadScene("Loading");
		PlayerPrefs.Save();
    }

    public IEnumerator Timer()
    {
        yield return new WaitForEndOfFrame();
        LevelScore.getInstance()._currentTimer = LevelScore.getInstance()._maxTimer;
        timerText.text = "Timer:\n" + LevelScore.getInstance()._currentTimer;
        while (LevelScore.getInstance()._currentTimer > 0)
        {
            LevelScore.getInstance()._currentTimer--;
            yield return new WaitForSeconds(1f);
            timerText.text = "Timer:\n" + LevelScore.getInstance()._currentTimer;
        }

        timerText.text = "Time up";
        yield return new WaitForSeconds(5f);
        //End of level code.
    }

    public void FinishLevel()
    {
        endingScoreCoroutine = StartCoroutine(EndingScoreOverlay());
    }
    private IEnumerator EndingScoreOverlay()
    {
        StopCoroutine(timerCoroutine);
        string s = endingScoreText.text;
		int total = LevelScore.getInstance().TotalScore;
		
		string grade = ScoreData.GetGrade(total);
		
        s = s.Replace("[all]", total.ToString());
		s = s.Replace("[grade]", " (" + grade + ")");
        s = s.Replace("[a]", LevelScore.getInstance().AccuracyScore.ToString());
        s = s.Replace("[e]", LevelScore.getInstance().EnemyScore.ToString());
        s = s.Replace("[t]", LevelScore.getInstance().TimerScore.ToString());
        s = s.Replace("[h]", LevelScore.getInstance().HealthScore.ToString());
        s = s.Replace("\r\n", "\n");
        string[] lines = s.Split('\n');

        endingScoreText.text = lines[0];

        float scaleY = 0f;
        float MOVE_PER_SECOND = 1f / 1.5f;  //Complete in 1.5 seconds.
        while(scaleY < 1f)
        {
            scaleY += Mathf.Clamp(MOVE_PER_SECOND * Time.deltaTime, 0.0f, 1f);

            endingScore.localScale = new Vector3(1f, scaleY, 1f);
            yield return new WaitForEndOfFrame();
        }

        for(int i = 1; i < lines.Length; i++)
        {
            yield return new WaitForSeconds(0.25f);
            AudioSource.PlayClipAtPoint(scoreFX, Camera.main.transform.position);
            endingScoreText.text = endingScoreText.text + "\n" + lines[i];
        }

        if (LevelVars.isDailyChallenge)
        {
            //Show the high score for daily challenge maybe maybe?
            if(total > PlayerPrefs.GetInt("DC_score"))
            {
                endingScoreText.text = endingScoreText.text + "\n" + "NEW HIGH SCORE";
                PlayerPrefs.SetInt("DC_score", total);
                PlayerPrefs.Save();
            }
        }
        else
        {
            if (ScoreData.instance().GetCurrentHighScore() < LevelScore.getInstance().TotalScore)
            {
                endingScoreText.text = endingScoreText.text + "\n" + "NEW HIGH SCORE";
            }
        }
        yield return new WaitForSeconds(0.5f);
        if (LevelVars.isDailyChallenge)
            returnToTitleButton.SetActive(true);
        else
            nextLevelButton.SetActive(true);
		replayLevelButton.SetActive(true);
        if (LevelVars.isDailyChallenge)
            EventSystem.current.SetSelectedGameObject(returnToTitleButton);
        else
            EventSystem.current.SetSelectedGameObject(nextLevelButton); 
        while (gameObject)
        {
            yield return new WaitForEndOfFrame();
        }
    }
}
