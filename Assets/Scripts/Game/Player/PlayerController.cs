﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {
    [SerializeField] protected RuntimeAnimatorController[] profiles;
    //public static int profileID = 1;
    [SerializeField] protected GameObject zapPrefab;
	private GameObject[] extensionZap;
    [SerializeField] protected AudioClip lavaFX, zapFX, dieFX;
    protected SpriteController control;
    protected Health h;

    protected Transform camTransform;
    float lavaTime = 0f;

	float[] powerupTime = {0f, 0f, 0f, 0f};
	public static float[] Powerup_Perc = {0f, 0f, 0f, 0f};
	public const float POWERUP_TIMER = 60f;
	private bool isDead = false;
	
	// Use this for initialization
	void Start () {
        if (control == null)
            control = GetComponent<SpriteController>();
        if (h == null)
            h = GetComponent<Health>();
		camTransform = Camera.main.transform;
        GetComponent<Animator>().runtimeAnimatorController = profiles[ScoreData.instance().profileID];
		
		for(int i = 0; i < powerupTime.Length; i++)
		{
			powerupTime[i] = 0f;
			Powerup_Perc[i] = 0f;
		}
		//Debug.Log("Remove these later");
		//StartPowerup(0);
		//StartPowerup(1);
		//StartPowerup(2);
		extensionZap = new GameObject[5];
		for(int i = 0; i < extensionZap.Length; i++)
		{
			extensionZap[i] = Instantiate(zapPrefab, transform.position, Quaternion.identity) as GameObject;
			extensionZap[i].name = "ZapPrefab";
		}
	}
	
	public void StartPowerup(int id)
	{
		powerupTime[id] = POWERUP_TIMER;
		Powerup_Perc[id] = 1f;
		
		if(id == Powerup.POWERUP_INVINCIBLE)
		{
			h.invincible = true;
		}
		if(id == Powerup.POWERUP_LONG_BEAM)
		{
			//Nothing to see here; move along
		}
		
		if(id == Powerup.POWERUP_SPEED)
		{
			control.speed = 8;
		}
        if (id == Powerup.POWERUP_ZEROG)
        {
            control.gravityScale = 0f;
        }
    }


	public void EndPowerup(int id)
	{
		if(id == Powerup.POWERUP_INVINCIBLE)
		{
			h.invincible = true;
		}
		if(id == Powerup.POWERUP_SPEED)
		{
			control.speed = 4;
		}
        if (id == Powerup.POWERUP_ZEROG)
        {
            control.gravityScale = 1f;
        }

        GameObject.FindWithTag("GameController").GetComponent<UIController>().EndPowerupText(id);
	}
	
	// Update is called once per frame
	void Update () {
        if (control == null)
            control = GetComponent<SpriteController>();
        if (h == null)
            h = GetComponent<Health>();
        if(camTransform == null)
            camTransform = Camera.main.transform;

        camTransform.position = transform.position + new Vector3(0f, 10f, -15f);

        Vector2 moveVec = new Vector2(CrossPlatformInputManager.GetAxisRaw("Horizontal"), CrossPlatformInputManager.GetAxisRaw("Vertical"));
        moveVec.Normalize();
        control.SetDirection(moveVec);

        //Make a zap[
        bool zap = false;
        /*
        if (Input.GetKeyDown(KeyCode.LeftControl))
            zap = true;
        if (Input.GetKeyDown(KeyCode.RightControl))
            zap = true;
        */
        if (CrossPlatformInputManager.GetButtonDown("Zap"))
            zap = true;

		//Speed up/down
		if(Input.GetKeyDown(KeyCode.Tab))
		{
			if(Time.timeScale > 0f)
				Time.timeScale = 2f;
		}
		if(Input.GetKeyUp(KeyCode.Tab))
		{
			if(Time.timeScale > 0f)
				Time.timeScale = 1f;
		}

        if (zap == true && zapPrefab.activeInHierarchy == false)
        {
			LevelScore.getInstance()._zaps++;
            AudioSource.PlayClipAtPoint(zapFX, camTransform.position);
            zapPrefab.SetActive(true);
            zapPrefab.transform.position = transform.position + (Vector3.up * 1f);
            Vector3 angles = zapPrefab.transform.eulerAngles;
            angles.y = control.angle + 90f;
            zapPrefab.transform.eulerAngles = angles;
            zapPrefab.transform.position = zapPrefab.transform.position + (zapPrefab.transform.forward * 3.0f);

            
			
			if(powerupTime[Powerup.POWERUP_LONG_BEAM] > 0f)
			{
				for(int i = 0; i < extensionZap.Length; i++)
				{
					extensionZap[i].SetActive(true);
					extensionZap[i].transform.position = transform.position + (Vector3.up * 1f);
					extensionZap[i].transform.eulerAngles = angles;
				}
				
				extensionZap[0].transform.position = extensionZap[0].transform.position + 
													(extensionZap[0].transform.forward * 3.0f) + (extensionZap[0].transform.right * 1f);
				extensionZap[1].transform.position = extensionZap[1].transform.position +
													(extensionZap[1].transform.forward * 3.0f) + (extensionZap[1].transform.right * -1f);
				extensionZap[2].transform.position = extensionZap[2].transform.position +
													(extensionZap[2].transform.forward * 6.0f);
				extensionZap[3].transform.position = extensionZap[3].transform.position +
													(extensionZap[3].transform.forward * 6.0f) + (extensionZap[3].transform.right * 1f);
				extensionZap[4].transform.position = extensionZap[4].transform.position + 
													(extensionZap[4].transform.forward * 6.0f) + (extensionZap[4].transform.right * -1f);
			}

         }

        //Get hurt by lava
        if(lavaTime > 0.25f)
        {
            AudioSource.PlayClipAtPoint(lavaFX, camTransform.position);
            lavaTime -= 0.25f;
            h.DamageOverride(5);
        }

        if(LevelScore.getInstance()._currentTimer <= 0)
        {
            LevelScore.getInstance()._currentTimer = 0;
            GetComponent<Health>().DamageOverride(100);
        }
		
		//extensionZapPrefab
		
		for(int i = 0; i < powerupTime.Length; i++)
		{
			if(powerupTime[i] > 0f)
			{
				powerupTime[i] -= Time.deltaTime;
				
				if(powerupTime[i] < 0f)
				{
					powerupTime[i] = 0f;
					EndPowerup(i);
				}
				
				Powerup_Perc[i] = powerupTime[i] / POWERUP_TIMER;
			}
			
		}
    }

    public void KillMe()
    {
		if(isDead == false)
		{
			isDead = true;
			StartCoroutine(KillRoutine());
		}
    }

    private IEnumerator KillRoutine()
    {
        AudioSource.PlayClipAtPoint(dieFX, Camera.main.transform.position);
        control.speed = 0;
        for (int i = 0; i < 4; i++)
        {
            control.SetDirection(Vector2.up);
            yield return new WaitForSeconds(0.15f);
            control.SetDirection(Vector2.right);
            yield return new WaitForSeconds(0.15f);
            control.SetDirection(Vector2.down);
            yield return new WaitForSeconds(0.15f);
            control.SetDirection(Vector2.left);
            yield return new WaitForSeconds(0.15f);
        }

        LoadingUI.levelToLoad = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        ControllerCollisionMessage msg = new ControllerCollisionMessage(gameObject, hit);
        hit.gameObject.SendMessage("OnHit", msg, SendMessageOptions.DontRequireReceiver);

        if (hit.gameObject.layer == LayerMask.NameToLayer("Lava"))
        {
            lavaTime += Time.deltaTime;
        }
    }
}
