﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelVars : MonoBehaviour {
    
    [SerializeField] private float _deathZ = -100;
    [SerializeField] private int _timer = 150;
    [SerializeField] private float _gravityScale = 1f, _skyboxRotationSpeed = 0f, _flashlightRadius = 0f;
    [SerializeField] private AudioClip musicForLevel;
    [SerializeField] private bool cannotFall = false;
    public static AudioSource musicSrc;

    public static float deathZ, gravityScale;
    private float currentSkyRotation = 0f;

    public static bool isDailyChallenge = false;

    // Use this for initialization
    void Start() {
        
        LevelScore.getInstance().ResetVars(_timer);
        deathZ = _deathZ;
        gravityScale = _gravityScale;

		if(_flashlightRadius > 0f)
		{
			GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteController>().SetLight(_flashlightRadius);
		}

        GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteController>().cannotFall = cannotFall;

        if (musicForLevel != null)
        {
            musicSrc = Camera.main.gameObject.AddComponent<AudioSource>();
            musicSrc.minDistance = 2f;
            musicSrc.loop = true;
            musicSrc.clip = musicForLevel;
            musicSrc.Play();
        }

	}
	
    
	// Update is called once per frame
	void Update () {
        //Rotate the sky sphere
        if (_skyboxRotationSpeed != 0f)
        {
            currentSkyRotation += _skyboxRotationSpeed * Time.deltaTime;
            if (currentSkyRotation > 360f)
                currentSkyRotation -= 360f;
            if (currentSkyRotation < 0f)
                currentSkyRotation += 360f;
            RenderSettings.skybox.SetFloat("_Rotation", currentSkyRotation);
        }
	}
}
