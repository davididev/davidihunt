﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialUI : MonoBehaviour {

    [SerializeField] private TMPro.TMP_Text tutorialOverlayText;
    [SerializeField] private Animator tutorialOverlayAnimator;

	// Use this for initialization
	void Start () {
        StartCoroutine(Tutorial());
	}

    IEnumerator Tutorial()
    {
        Transform player = GameObject.FindWithTag("Player").transform;
        Health playerHealth = player.GetComponent<Health>();
        Vector3 startingPos = player.position;
        tutorialOverlayText.text = "Press arrow keys to move.";
        while(Vector3.Distance(startingPos, player.position) < 2.5f)
        {
            yield return new WaitForEndOfFrame();
        }


        tutorialOverlayText.text = "Press Ctrl to fire.";
        bool fire = false;
        while(fire == false)
        {
            if (Input.GetKey(KeyCode.LeftControl))
                fire = true;
            if (Input.GetKey(KeyCode.RightControl))
                fire = true;

            yield return new WaitForEndOfFrame();
        }

        tutorialOverlayText.text = "Look for the green flag to beat the level.";
        yield return new WaitForSeconds(5f);

        tutorialOverlayAnimator.SetTrigger("Display Score");
        float healthTimer = 0f;
        while(gameObject)
        {
            tutorialOverlayText.text = "Score: " + LevelScore.getInstance().TotalScore.ToString("D3")
                + "\nAccuracy Score: " + LevelScore.getInstance().AccuracyScore.ToString("D3")
                + "\nEnemy Score: " + LevelScore.getInstance().EnemyScore.ToString("D3")
                + "\nTime Score: " + LevelScore.getInstance().TimerScore.ToString("D3")
                + "\nHealth Score: " + LevelScore.getInstance().HealthScore.ToString("D3");

            yield return new WaitForEndOfFrame();
            healthTimer += Time.deltaTime;
            if (healthTimer > 10f)
            {
                playerHealth.DamageOverride(-100);
                healthTimer = 0f;
            }

            
        }
    }

    // Update is called once per frame
    void Update () {
        if (LevelScore.getInstance()._currentTimer < 5)
            LevelScore.getInstance()._currentTimer = 5;
    }
}
