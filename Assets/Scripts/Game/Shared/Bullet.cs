﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    [SerializeField] private LayerMask toHitMask;
    [SerializeField] private bool piercing = false;
    [SerializeField] private float moveSpeed = 0f;
    private Rigidbody rigid;
    private BoxCollider col;
    public int damage = 10;
    float damageTimer = 0f, lifespanTimer = 0f;
    [SerializeField] float damageDelay = 0.5f, lifeSpan = 5f;


    // Use this for initialization
    void OnEnable () {
        lifespanTimer = lifeSpan;

    }

    
	
	// Update is called once per frame
	void FixedUpdate () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (col == null)
           col = GetComponent<BoxCollider>();



        rigid.velocity = transform.forward * moveSpeed;

        if(lifespanTimer > 0f)
        {
            lifespanTimer -= Time.fixedDeltaTime;
            if (lifespanTimer <= 0f)
                gameObject.SetActive(false);
        }

        if (damageTimer <= 0f)
        {
            Collider[] clist = PhysicsExtensions.OverlapBox(col, toHitMask);
            foreach (Collider c in clist)
            {
                Health h = c.GetComponent<Health>();
                h.Damage(damage);

                if (gameObject.name == "ZapPrefab")
                {
                    LevelScore.getInstance()._successZaps++;
					Debug.Log("Zaps: " + LevelScore.getInstance()._zaps + "/" + LevelScore.getInstance()._successZaps);
                }
            }
            if (clist.Length > 0)
            {
                if (piercing == false)
                    gameObject.SetActive(false);
            }
            damageTimer = damageDelay;
        }
        else
            damageTimer -= Time.deltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Default"))
            gameObject.SetActive(false);
    }
}
