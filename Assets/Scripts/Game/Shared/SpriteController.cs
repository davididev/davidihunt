﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteController : MonoBehaviour {

    protected CharacterController cc;
    protected Animator anim;
    protected Vector2 currentDirection, lastDir;
    public bool lockFacing = false, cannotFall = false;
	public float gravityScale = 1f;
    int facingID = 1;


    public bool isGrounded
    {
        get
        {
            return cc.isGrounded;
        }
    }
    /// <summary>
    /// Get an angle representation of the last Direction
    /// </summary>
    public float angle
    {
        get
        {
            return Mathf.Atan2(-lastDir.y, lastDir.x) * Mathf.Rad2Deg;
        }
    }

    public float speed = 4; 

    /// <summary>
    /// Get last Direction in Vector3 format
    /// </summary>
    /// <returns></returns>
    public Vector3 GetDirectionRay()
    {
        return new Vector3(lastDir.x, 0f, lastDir.y);
    }


    /// <summary>
    /// Jump
    /// </summary>
    /// <param name="height"></param>
    public void Jump(float height)
    {
        jumpHeightRemaining = height;
        jumpForce = 0f;
    }
    float jumpForce = -1f;
    float jumpHeightRemaining = 0f;

    /// <summary>
    /// Get facing ray based on facing (accounting for Locked Facing too)
    /// </summary>
    /// <returns></returns>
    public Vector3 GetForwardRay()
    {
        if (facingID == 1)
            return new Vector3(0f, 0f, 1f);
        if (facingID == 2)
            return new Vector3(1f, 0f, 0f);
        if (facingID == 3)
            return new Vector3(0f, 0f, -1f);
        if (facingID == 4)
            return new Vector3(-1f, 0f, 0f);

        return Vector3.zero;
    }
	
	public void SetLight(float radius)
	{
		GameObject g = new GameObject("Lamp");
		g.transform.parent = transform;
		g.transform.localPosition = new Vector3(0f, 3f, -1f);
		Light l = g.AddComponent<Light>();
		l.range = radius;
	}

    /// <summary>
    /// Set the facing
    /// </summary>
    /// <param name="v"></param>
    public void SetDirection(Vector2 v)
    {
        currentDirection = v;
        int lol = 0;
        if (v.y == 0f)
        {
            if (v.x == 0)
                lol = 0;
            if (v.x < 0f)
                lol = 4;
            if (v.x > 0f)
                lol = 2;
        }
        if (v.y > 0f)
            lol = 1;
        if (v.y < 0f)
            lol = 3;
        if (lockFacing == false)
            facingID = lol;
        anim.SetInteger("Walk Dir", facingID);
    }

	// Use this for initialization
	void Awake() {
            cc = GetComponent<CharacterController>();
            anim = GetComponent<Animator>();
    }

    float gravity = 0f;
	// Update is called once per frame
	void FixedUpdate () {
        if (cc == null)
            cc = GetComponent<CharacterController>();
        if (anim == null)
            anim = GetComponent<Animator>();

        gravity = gravity - (1.5f * LevelVars.gravityScale * this.gravityScale * Time.fixedDeltaTime);
        if (cc.isGrounded)
            gravity = -.1f * LevelVars.gravityScale * this.gravityScale;
        //HAndle jump
        if (jumpForce >= 0f)
        {
            gravity = 0f;
            jumpForce = jumpForce + (1.5f * LevelVars.gravityScale * this.gravityScale * Time.fixedDeltaTime);
            jumpHeightRemaining -= jumpForce;

            cc.Move(Vector3.up * jumpForce);

            if (jumpHeightRemaining <= 0f)
                jumpForce = -1f;  //End jump

            //Debug.Log("Jump:  remain (" + jumpHeightRemaining + ").  Force: " + jumpForce);
        }

        if (currentDirection == Vector2.zero)
            anim.SetFloat("Time Scale", 0f);
        else
        {
            anim.SetFloat("Time Scale", speed / 4f);
            lastDir = currentDirection;  //LastDir should NEVER be 0,0
        }

        Vector3 movement = Vector3.zero;
        
		

        movement.y = gravity;
        movement.x = currentDirection.x * speed * Time.fixedDeltaTime;
        movement.z = currentDirection.y * speed * Time.fixedDeltaTime;

        if (cannotFall)
        {
			Vector3 previousMove = movement;
            if (movement.x != 0f)
			{
                if (!WontFallHere(new Vector3(movement.normalized.x, 0f, 0f)))
                    movement.x = 0f;
            }
            if (movement.z != 0f)
            {
                if (!WontFallHere(new Vector3(0f, 0f, movement.normalized.z)))
                    movement.z = 0f;
            }
			
			//Uh oh, we got stuck because we hvae to move diagnolly
			/*
			if(movement.x == 0f && movement.z == 0f)
			{
				//Debug.Log("Stuck!");
				Vector3 v = new Vector3();
				if(previousMove.x < 0f)
					v.x = -1f;
				if(previousMove.x > 0f)
					v.x = 1f;
				if(previousMove.z < 0f)
					v.z = -1f;
				if(previousMove.z > 0f)
					v.z = 1f;
				if(WontFallHere(new Vector3(v.x, 0f, v.z)))
				{
					movement = previousMove;
				}
			}
			*/

        }

        cc.Move(movement);
    }

    /// <summary>
    /// Internal function; check around offset and 1 unit down to see if you'll fall.
    /// </summary>
    /// <param name="offset">Should only set x/z</param>
    /// <returns>True if there is footing; false if not.</returns>
    bool WontFallHere(Vector3 offset)
    {
        //Note: to climb uphill, you have to start the trace 1 unit up
        //It's best to check 1/2 of a unit to the left/right/forward/back
        Vector3 scanPos = transform.position + (offset * 0.5f) + (1.75f * Vector3.up);
		Vector3 closeScanPos = transform.position + (offset * 0.25f) + (1.75f * Vector3.up);
        if (Physics.Raycast(scanPos, Vector3.down, (1.15f * 3f), LayerMask.GetMask("Default")))
        {
            return true;
        }
		if (Physics.Raycast(closeScanPos, Vector3.down, (1.15f * 3f), LayerMask.GetMask("Default")))
        {
            return true;
        }
		


        return false;
    }
}

