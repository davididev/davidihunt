﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {
    [SerializeField] protected int startingHealth = 100;
    [SerializeField] protected float damageDelay = 2f;
    [SerializeField] protected UnityEvent onDie, onHitHurt, onHitBlock, onHeal;
    [SerializeField] protected bool inactiveOnDeath = true;

    private Coroutine redFlash;
    public bool invincible = false;
    float damageTimer = 0f;
    int currentHealth;
    SpriteRenderer rend;

    public float healthPerc
    {
        get
        {
            return (float)currentHealth / (float)startingHealth;
        }
    }

	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {
        currentHealth = startingHealth;
    }

    // Update is called once per frame
    void Update () {
        if (rend == null)
            rend = GetComponent<SpriteRenderer>();

        if(gameObject.name == "Player")
        {
            LevelScore.getInstance()._health = currentHealth;
        }
        if (damageTimer > 0f)
            damageTimer -= Time.deltaTime;

        if (transform.position.y < LevelVars.deathZ)
        {
            Enemy e = GetComponent<Enemy>();
            if (e != null)
                LevelScore.getInstance()._enemiesSlain -= e.addedWeight;
            DamageOverride(90000);  //KILL FOR FALLING TOO FAR C:<
        }
    }

    private IEnumerator RedFlash()
    {
        if (rend == null)
            rend = GetComponent<SpriteRenderer>();

        rend.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        rend.material.color = Color.white;
    }

    private IEnumerator GreenFlash()
    {
        if (rend == null)
            rend = GetComponent<SpriteRenderer>();

        rend.material.color = Color.green;
        yield return new WaitForSeconds(0.1f);
        rend.material.color = Color.white;
    }

    public void Damage(int amount)
    {
        if (invincible)
            onHitBlock.Invoke();
        else
        {
            if(damageTimer <= 0f)
                DamageOverride(amount);

            damageTimer = damageDelay;
        }
    }

    public void DamageOverride(int amount)
    {
        currentHealth -= amount;
        if (amount > 0)
        {
            if (currentHealth <= 0f)
            {
                onDie.Invoke();
                if(inactiveOnDeath)
                    gameObject.SetActive(false);
            }
            else
            {
                onHitHurt.Invoke();
                if (redFlash != null)
                    StopCoroutine(redFlash);
                redFlash = StartCoroutine(RedFlash());
            }
        }
        else
        {
            if(amount < 0)  //Heal
            {
                if (currentHealth > startingHealth)
                    currentHealth = startingHealth;
                onHeal.Invoke();
                if (redFlash != null)
                    StopCoroutine(redFlash);
                redFlash = StartCoroutine(GreenFlash());
            }
        }
        


        
    }
}
