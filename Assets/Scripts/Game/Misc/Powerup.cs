﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

	[SerializeField] int powerupType = 0;
	[SerializeField] AudioClip powerupSound;
	
	public const int POWERUP_INVINCIBLE = 0;
	public const int POWERUP_LONG_BEAM = 1;
	public const int POWERUP_SPEED = 2;
    public const int POWERUP_ZEROG = 3;

    public static string GetPowerupString(int id)
	{
		string pu = "Powerup not found";
		
		if(id == POWERUP_INVINCIBLE)
			pu = "Invincibility";
		if(id == POWERUP_LONG_BEAM)
			pu = "Long Beam";
		if(id == POWERUP_SPEED)
			pu = "Speed";
        if (id == POWERUP_ZEROG)
            pu = "Zero Gravity";
        return pu;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Player")  //I don't see why it'd be anything else due to project settings, but just in case...
		{
			col.gameObject.SendMessage("StartPowerup", powerupType);
			AudioSource.PlayClipAtPoint(powerupSound, Camera.main.transform.position);
			
			GameObject.FindWithTag("GameController").GetComponent<UIController>().ShowPowerupText(powerupType);
            if(powerupType != POWERUP_ZEROG)  //Zero gravity doesn't disappear
                gameObject.SetActive(false);
		}
	}
}
