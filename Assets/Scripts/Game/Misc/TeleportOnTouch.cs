﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOnTouch : MonoBehaviour {

    public LayerMask maskToTeleport;
    public Vector3 teleportPosition;
	public Transform pieceToTeleportTo;  
    public bool relative = false;
	public AudioClip teleportSound;
	
	private static bool teleported = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator TeleportDelay()
	{
		teleported = true;
		for(int i = 0; i < 3; i++)
		{
			yield return new WaitForEndOfFrame();
		}
		teleported = false;
		
	}
	
    private void OnTriggerEnter(Collider other)
    {
		if(teleported == true)
			return;
		
		StartCoroutine(TeleportDelay());  //To avoid a perpetual loop of TriggerEnters, we're going to set teleported flag to "true" for a couple of frame steps.
        int l = other.gameObject.layer;
        if(Contains(maskToTeleport, l))
        {
			Vector3 v;
			if(relative)
			{
				if(pieceToTeleportTo != null)
					v = transform.position + pieceToTeleportTo.position;
				else
					v = transform.position + teleportPosition;
			}
			else
			{
				if(pieceToTeleportTo != null)
					v = pieceToTeleportTo.position;
				else
					v = teleportPosition;
			}
            CharacterController cc = other.GetComponent<CharacterController>();
            if (cc != null)
                cc.enabled = false;
            other.transform.position = v;
            if (cc != null)
                cc.enabled = true;
			
			if(teleportSound != null)
			{
				AudioSource.PlayClipAtPoint(teleportSound, Camera.main.transform.position);
			}
        }
    }

    public bool Contains(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}
