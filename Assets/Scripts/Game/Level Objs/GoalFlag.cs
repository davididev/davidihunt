﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalFlag : MonoBehaviour {

    private bool touched = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(touched == false)
            {
                touched = true;
                GameObject.FindWithTag("GameController").GetComponent<UIController>().FinishLevel();
            }
        }
    }
}
