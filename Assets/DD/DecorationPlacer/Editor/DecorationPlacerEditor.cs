﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DecorationPlacer))]
public class DecorationPlacerEditor : Editor
{
    int errors = 0;
    int totalWeight = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Pool of decorations:");
        EditorGUILayout.LabelField("Weight and scale should be more than 0.");
        base.OnInspectorGUI();

        DecorationPlacer settings = (DecorationPlacer)target;

        EditorGUILayout.LabelField("Settings: ");
        settings.wallMode = EditorGUILayout.Toggle("Wall mode", settings.wallMode);
        settings.distanceFromSurface = EditorGUILayout.FloatField("Distance from surface", settings.distanceFromSurface);


        totalWeight = 0;
        errors = 0;
		if(settings.possibleDecorations != null)
		{
			for (int i = 0; i < settings.possibleDecorations.Length; i++)
			{
				if (settings.possibleDecorations[i].weight == 0)
					errors++;
				if (settings.possibleDecorations[i].minScale == 0f)
					errors++;
				if (settings.possibleDecorations[i].maxScale == 0f)
					errors++;
				totalWeight += settings.possibleDecorations[i].weight;
			}
		}
        
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Hold down Ctrl and click on the scene to add.");
        if(errors > 0 )
        {
            EditorGUILayout.LabelField("Check your weight/scale in the decorations.");
            EditorGUILayout.LabelField(errors + " errors found.");
        }
    }

    void OnSceneGUI()
    {
        if (errors > 0)
            return;
        DecorationPlacer settings = (DecorationPlacer)target;


        if (Event.current.control)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }

        if (Event.current.control && Event.current.type == EventType.MouseDown)
        {
            //Pick a random object
            int w = Random.Range(1, totalWeight);
            int id = 0;
            while (w > 0)
            {
                w -= settings.possibleDecorations[id].weight;
                if (w >= 0)
                    id++;
            }

            float scale = Random.Range(settings.possibleDecorations[id].minScale, settings.possibleDecorations[id].maxScale);

            //Place object
            GameObject prefab = settings.possibleDecorations[id].prefab;

            

                //TODO: add bounds
                float offset = 0f;


            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 250f, settings.placeOnThese.value))
            {
                //GameObject copyOfPrefab = PrefabUtility.GetPrefabParent(prefab) as GameObject;
                GameObject g = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                //GameObject g = GameObject.Instantiate(prefab, hit.point, Quaternion.identity) as GameObject;
                //GameObject g = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
                g.transform.position = hit.point;


                Vector3 r = settings.possibleDecorations[id].randomRotation;
                Vector3 rot = new Vector3(Random.Range(-r.x, r.x), Random.Range(-r.y, r.y), Random.Range(-r.z, r.z));
                g.transform.eulerAngles = g.transform.eulerAngles + rot;

                g.transform.parent = settings.transform;
                Vector3 ps = prefab.transform.localScale;  //Prefab scale.  Scale of object should be relevant to prefab scale.
                g.transform.localScale = new Vector3(scale * ps.x, scale * ps.y, scale * ps.z);

                

				Vector3 pos = Vector3.zero;
                if(settings.wallMode)
				{
                    pos = g.transform.position + (Vector3.back * (settings.distanceFromSurface + 0.1f));
					pos.x = Mathf.Round(pos.x * 2f) / 2f;
					pos.y = Mathf.Round(pos.y * 2f) / 2f;
				}
                else
				{
					pos = g.transform.position + (g.transform.up * (settings.distanceFromSurface));
					pos.x = Mathf.Round(pos.x * 2f) / 2f;
					pos.z = Mathf.Round(pos.z * 2f) / 2f;

				}
				g.transform.position = pos;
            }
        }
    }
}
