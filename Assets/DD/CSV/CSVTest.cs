﻿using UnityEngine;
using System.Collections;

/// <summary>
/// For this demonstration we are writing a Pineapple and Coconut instance to a file
/// and then reading it.  
/// To test "write", make sure the inspector checkmark "write" is turned on and press play.
/// To test "read", make sure the inspector checkmark "write" is turned off and press play.
/// All of this will be demonstrated in the console.
/// 
/// The CSV system will try to pass an object as an int first, a float second, and a string third.
/// 
/// If you want the file hidden, be sure to use Application.persistentDataPath + "/fileDir/"
/// It's also required on some platforms, such as Android.
/// 
/// *******************************************************************************
/// EDITORS NOTE: if a string contains a ',' the comma is replaced with "&c;"  
/// This is fine for plain text files but be wary if you are using encryption code
/// as it might create a &c; somewhere where you didn't place a comma.
/// *******************************************************************************
/// </summary>
public class CSVTest : MonoBehaviour {

    private class Pineapple
    {
        public bool isYummy = true;
        public int leaves = 4;
    }

    private Pineapple pine = new Pineapple();

    private class Coconut
    {
        public float oz = 2.5f;
    }
    private Coconut coke = new Coconut();

    public bool write = true;

	// Use this for initialization
	void Start () {
        if (write)
            WriteFile();
        else
            ReadFile();
	}
	
    public void WriteFile()
    {
        CSVWriter.Reset();
        CSVWriter.WriteLine(new object[3] { "Pineapple", pine.isYummy, pine.leaves} );
        CSVWriter.WriteLine(new object[2] { "Coconut", coke.oz});
        CSVWriter.SaveFile("testData.csv");
    }

    public void ReadFile()
    {
        string[] lines = CSVReader.GetFile("testData.csv");
        for(int i = 0; i < lines.Length; i++)
        {
            object[] args = CSVReader.GetArgumentsInLine(lines[i]);
            string className = args[0] as string;
            if(className == "Pineapple")
            {
                pine.isYummy = (bool) args[1];
                pine.leaves = (int) args[2];
                Debug.Log("read a pineapple.  isYummy=" + pine.isYummy + "; leaves:" + pine.leaves);
            }
            if(className == "Coconut")
            {
                coke.oz = (float) args[1];
                Debug.Log("read a coconut.  oz=" + coke.oz);
            }
        }
        
    }
    
}